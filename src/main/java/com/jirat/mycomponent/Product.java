/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author ACER
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "ลาเต้ 1",40, "1.png"));
        list.add(new Product(2, "ลาเต้ 2",30, "1.png"));
        list.add(new Product(3, "ลาเต้ 3",40, "1.png"));
        list.add(new Product(4, "โกโก้ 1",30, "2.png"));
        list.add(new Product(5, "โกโก้ 2",40, "2.png"));
        list.add(new Product(6, "โกโก้ 3",50, "2.png"));
        list.add(new Product(7, "มัทฉะลาเต้ 1",45, "3.png"));
        list.add(new Product(8, "มัทฉะลาเต้ 2",50, "3.png"));
        list.add(new Product(9, "มัทฉะลาเต้ 3",55, "3.png"));
        return list;
    }
}
